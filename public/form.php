<!DOCTYPE html>

<html lang="en">
	<head>
		<title>Задание 8. Щеглов Ярослав</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="style.css">
	</head>

	<body>
		<a href="admin.php">Админ ссылка на вход в Базу Данных</a>
		<header>Форма</header>
		<?php
			if(!empty($messages['login_and_password'])){
				print($messages['login_and_password']);
			}
			if (!empty($messages['save'])) {
				print($messages['save']);
			}
			error_reporting(E_ALL & ~E_NOTICE);
		?>
		<form method="post" action="index.php"> 
		<!--1-->
		<label>
			<?php
				$ERROR='';
				$name='';
				if (!empty($messages['name'])) {
					print($messages['name']);
					$ERROR='error';
				}
				if(!empty($values['name'])){
					$name=$values['name'];
				}
			?>
			Имя: <input type="text" name="name" placeholder="Введите имя" class="<?php print $ERROR?>" value="<?php print $name?>">
		</label>
	
		<!--2-->
		<label>
			<?php
				$ERROR='';
				$email='';
				if (!empty($messages['email'])) {
					print($messages['email']);
					$ERROR='error';
				}
				if(!empty($values['email'])){
					$email=$values['email'];
				}
			?>
			Email: <input type="email" name="email" placeholder="Введите email" value="<?php print $email?>" class="<?php print $ERROR?>">
		</label>

		<!--3-->
		<label>
			<?php
				$ERROR='';
				if (!empty($messages['date'])) {
					print($messages['date']);
					$ERROR='error';
				} 
			?>
			Дата рождения: <input type="date" name="date" value="<?php $date='';if(!empty($values['date'])){$date=$values['date'];} print $date?>" class="<?php print $ERROR?>">
		</label>

		<!--4-->
		<label>
			<?php
				$ERROR='';
				if (!empty($messages['sex'])) {
					print($messages['sex']);
					$ERROR='error';
				}
			?>
			Пол: 
			<input type="radio" name="sex" value="Мужской" <?php if($values['sex']=='Мужской') {print'checked';}?>> Мужской
			<input type="radio" name="sex" value="Женский" <?php if($values['sex']=='Женский') {print'checked';}?>> Женский
		</label>
	
		<!--5-->
		<label>
			<?php
				$ERROR='';
				if (!empty($messages['countlimbs'])) {
					print($messages['countlimbs']);
					$ERROR='error';
				}
			?>
			Кол-во конечностей:
			<?php
				$select_limbs=array(1=>'',2=>'',2=>'',3=>'',4=>'');
				for($s=1;$s<=4;$s++){
					if($values['countlimbs']==$s){
						$select_limbs[$s]='checked';break;
					}
				}
			?>
			<span class="<?php print $ERROR?>">
				<input type="radio" value="1" name="countlimbs" <?php print $select_limbs[1]?>>1
				<input type="radio" value="2" name="countlimbs" <?php print $select_limbs[2]?>>2
				<input type="radio" value="3" name="countlimbs" <?php print $select_limbs[3]?>>3
				<input type="radio" value="4" name="countlimbs" <?php print $select_limbs[4]?>>4
			</span>
		</label>
	
		<!--6-->
		<label>
			<?php
				$ERROR='';
				if(!empty($messages['super'])){
					print($messages['super']);
					$ERROR='error';
				}
			?>
			<span>
				Сверхспособности:</br>
				<?php
					if(!empty($values['super'])){
						$flag=FALSE;
						$SUPER_PROVERKA = array("net" =>"", "unvisibility" =>"", "wallhack" =>"", "mindcontrol" =>"", "changebody" =>"", "changereality" =>"");
						$SUPER = unserialize($values['super']);
						if(!empty($SUPER))foreach ($SUPER as $E){
							if($E=="net"){
								$SUPER_PROVERKA["net"]="selected";
								$flag=TRUE;break;
							}
						}
						if(!empty($SUPER)){
							if(!$flag)foreach ($SUPER as $T){
								$SUPER_PROVERKA["$T"]="selected";
							}
						}
					}
				?>
				<select id="sposobnost" name="super[]" multiple="multiple" size="3" class="<?php print $ERROR?>">
					<option value="net" <?php if(!empty($values['super'])) print $SUPER_PROVERKA["net"]?>>Нету</option>
					<option value="unvisibility"<?php if(!empty($values['super'])) print $SUPER_PROVERKA["unvisibility"]?>>Невидимость</option>
					<option value="wallhack"<?php if(!empty($values['super'])) print $SUPER_PROVERKA["wallhack"]?>>Смотреть сквозь стены</option>
					<option value="mindcontrol"<?php if(!empty($values['super'])) print $SUPER_PROVERKA["mindcontrol"]?>>Контроль разума</option>
					<option value="changebody"<?php if(!empty($values['super'])) print $SUPER_PROVERKA["changebody"]?>>Способности метаморфа</option>
					<option value="changereality"<?php if(!empty($values['super'])) print $SUPER_PROVERKA["changereality"]?>>Изменение рельности</option>
				</select>
			</span>
		</label>
	
		<!--7-->
		<label>
			<?php
				$ERROR='';
				$BIO='';
				if (!empty($messages['biography'])) {
					print($messages['biography']);
					$ERROR='error';
				}
				if(!empty($values['biography'])){
					$BIO=$values['biography'];
				}
			?>
			<p class="<?php print $ERROR?>" >
				<textarea cols="50" name="biography" placeholder="Расскажите о себе"><?php if($BIO!='')print $BIO;?></textarea>
			</p>
		</label>

		<!--8-->
		<label>
			<?php
				$ERROR='';
				if (!empty($messages['checkvajno'])) {
					print($messages['checkvajno']);
					$ERROR='error';
				}
			?>
			<span class="<?php print $ERROR?>" >
				С контрактом ознакомлен <input type="checkbox" name="checkvajno"  value="yes" <?php if($values['checkvajno']=='yes') {print'checked';}?>>
			</span>
		</label>

		<!--9-->
		<label>
			<button type="submit"> Отправить </button>
		</label>
		</form>
	</body>
	<!--The End-->
</html> 
